/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.ORM;

import System.Bootstrap;
import java.sql.Connection;
import java.util.Vector;

/**
 *
 * @author Hamza
 * @param <T>
 */
public abstract class ORM< T> {

    Bootstrap app = Bootstrap.getInstance();
    Connection con = app.getDBConnection();


    /**
     * Get an Object by ID
     *
     * @param id
     * @return
     */
    public abstract T find(int id);
    
    /**
     * Get an Object by ID
     *
     * @param id
     * @return
     */
    public abstract Vector<T> list();

    /**
     * Create an Object & Store it in DB
     *
     * @param obj
     * @return 
     */
    public abstract T create(T obj);

    /**
     * Update an Object
     *
     * @param obj
     * @return 
     */
    public abstract T update(T obj);

    /**
     * Delete an Object
     *
     * @param id
     */
    public abstract void delete(int id);

}
