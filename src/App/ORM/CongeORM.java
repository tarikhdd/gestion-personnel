/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.ORM;

import App.Models.Conge;
import App.Models.Employe;
import App.Models.Categorie;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author taik
 */
public class CongeORM extends ORM<Conge>{
    
    public CongeORM() {  }
  
    @Override
    public Conge find(int id) {
        Conge co= new Conge();
        try {
            PreparedStatement ps = con.prepareStatement("select c.*,e.* from conge as c left join employe e on e.idEmploye=c.idEmploye where c.idConge=? ");
            ps.setInt(1,id);
            ResultSet rs = ps.executeQuery();
            
            if (rs.next()) {
                co = new Conge(
                        rs.getInt("idConge"),
                        new Employe(
                            rs.getInt("idEmploye"),
                                rs.getString("Nom"),
                                rs.getString("Prenom"),
                                new Date(rs.getDate("DateNaissance").getTime()),
                                new Date(rs.getDate("DateEntree").getTime()),
                                new Date(rs.getDate("DateSortie").getTime()),
                                new Categorie()
                        ),
                        new Date(rs.getDate("DateDebut").getTime()),
                        new Date(rs.getDate("DateFin").getTime())
                );
            }
            ps.close();
        } catch (Exception ex) {
            Logger.getLogger(EmployeORM.class.getName()).log(Level.SEVERE, null, ex);
        }
        return co;
    }

    @Override
    public Vector<Conge> list() {
        Vector<Conge> vvl = new Vector<Conge>();
        try {
            PreparedStatement ps = con.prepareStatement("select c.*,e.* from conge as c left join employe e on e.idEmploye=c.idEmploye  ");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                vvl.add(new Conge(
                        rs.getInt("idConge"),
                        new Employe(
                            rs.getInt("idEmploye"),
                                rs.getString("Nom"),
                                rs.getString("Prenom"),
                                new Date(rs.getDate("DateNaissance").getTime()),
                                new Date(rs.getDate("DateEntree").getTime()),
                                new Date(rs.getDate("DateSortie").getTime()),
                                new Categorie()
                        ),
                        new Date(rs.getDate("DateDebut").getTime()),
                        new Date(rs.getDate("DateFin").getTime())
                ));
            }
            ps.close();
        } catch (Exception ex) {
            Logger.getLogger(EmployeORM.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vvl;
    }
    
    /**
     * Retourner la liste des congés d'un certain employé
     * @param id
     * @return 
     */
    public Vector<Conge> listByEmploye(int id) {
                Vector<Conge> vvl = new Vector<Conge>();
        try {
            PreparedStatement ps = con.prepareStatement("select c.*,e.* from conge as c left join employe e on e.idEmploye=c.idEmploye where c.idEmploye=? ");
            ps.setInt(1,id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
            vvl.add(new Conge(
                        rs.getInt("idConge"),
                        new Employe(
                            rs.getInt("idEmploye"),
                                rs.getString("Nom"),
                                rs.getString("Prenom"),
                                new Date(rs.getDate("DateNaissance").getTime()),
                                new Date(rs.getDate("DateEntree").getTime()),
                                new Date(rs.getDate("DateSortie").getTime()),
                                new Categorie()
                        ),
                        new Date(rs.getDate("DateDebut").getTime()),
                        new Date(rs.getDate("DateFin").getTime())
                ));
            }
            ps.close();
        } catch (Exception ex) {
            Logger.getLogger(EmployeORM.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vvl;
    }
    @Override
    public Conge create(Conge obj) {
        try {
            PreparedStatement ps = con.prepareStatement("insert into conge(DateDebut,DateFin) values(?,?)");
            ps.setDate(1, new Date(obj.getDateDebut().getTime()));
            ps.setDate(2, new Date(obj.getDateFin().getTime()));
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
        }
        return null;
    }

    @Override
    public Conge update(Conge obj) {
        try {
            PreparedStatement ps = con.prepareStatement("update conge set DateDebut=?,DateFin=?");
            ps.setDate(1, new Date(obj.getDateDebut().getTime()));
            ps.setDate(2, new Date(obj.getDateFin().getTime()));
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
        }
        return null;
    }

    @Override
    public void delete(int id) {
        try {

            PreparedStatement ps = con.prepareStatement("delete from  conge where idConge=?");
            ps.setInt(1, id);
            int nb = ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
        }
    }
    
}
