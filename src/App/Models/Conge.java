/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.Models;

import java.util.Date;

/**
 *
 * @author taik
 */
public class Conge {
    private int idConge;
    private Date DateDebut;
    private Date DateFin;
    private Employe employe;

    public Conge() {
    }
    
    
    public Conge(int idConge, Date DateDebut, Date DateFin) {
        this.idConge = idConge;
        this.DateDebut = DateDebut;
        this.DateFin = DateFin;
    }

    public Conge(int idConge, Employe employe, Date DateDebut, Date DateFin) {
        this.idConge = idConge;
        this.DateDebut = DateDebut;
        this.DateFin = DateFin;
        this.employe = employe;
    }

    public Employe getEmploye() {
        return employe;
    }

    public void setEmploye(Employe idEmploye) {
        this.employe = idEmploye;
    }
    


    public int getIdConge() {
        return idConge;
    }

    public Date getDateDebut() {
        return DateDebut;
    }

    public Date getDateFin() {
        return DateFin;
    }

    public void setIdConge(int idConge) {
        this.idConge = idConge;
    }

    public void setDateDebut(Date DateDebut) {
        this.DateDebut = DateDebut;
    }

    public void setDateFin(Date DateFin) {
        this.DateFin = DateFin;
    }

    @Override
    public String toString() {
        return "Conge{" + "idConge=" + idConge + ", DateDebut=" + DateDebut + ", DateFin=" + DateFin + ", Employe=" + employe + '}';
    }




    
}
