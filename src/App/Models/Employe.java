/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.Models;

import java.util.Date;

/**
 *
 * @author taik
 */
public class Employe {
    /* encapsulation DONE!! From*/
    private int idEmploye;
    private String Nom;
    private String Prenom;
    private Date DateNaissance;
    private Date DateEntree;
    private Date DateSortie;
    private Categorie categorie;
/* To-----------------------------------------*/
    public Employe(int idEmploye, String Nom, String Prenom, Date DateNaissance, Date DateEntree, Date DateSortie,Categorie categorie) {
        this.idEmploye = idEmploye;
        this.Nom = Nom;
        this.Prenom = Prenom;
        this.DateNaissance = DateNaissance;
        this.DateEntree = DateEntree;
        this.DateSortie = DateSortie;
        this.categorie=categorie;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }


    public Employe() {
    }

    public int getIdEmploye() {
        return idEmploye;
    }

    public String getNom() {
        return Nom;
    }

    public String getPrenom() {
        return Prenom;
    }

    public Date getDateNaissance() {
        return DateNaissance;
    }
    
    public Date getDateEntree() {
        return DateEntree;
    }

    public Date getDateSortie() {
        return DateSortie;
    }

    public void setIdEmploye(int idEmploye) {
        this.idEmploye = idEmploye;
    }

    public void setNom(String Nom) {
        this.Nom = Nom;
    }

    public void setPrenom(String Prenom) {
        this.Prenom = Prenom;
    }

    public void setDateNaissance(Date DateNaissance) {
        this.DateNaissance = DateNaissance;
    }

    public void setDateEntree(Date DateEntree) {
        this.DateEntree = DateEntree;
    }

    public void setDateSortie(Date DateSortie) {
        this.DateSortie = DateSortie;
    }

    @Override
    public String toString() {
        return "Employe{" + "idEmploye=" + idEmploye + ", Nom=" + Nom + ", Prenom=" + Prenom + ", DateNaissance=" + DateNaissance + ", DateEntree=" + DateEntree + ", DateSortie=" + DateSortie + ", categorie=" + categorie + '}';
    }

    
    
}
