/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tests;

import App.Models.Conge;
import App.Models.Employe;
import App.ORM.CongeORM;
import App.ORM.EmployeORM;
import App.ORM.EmployeORM;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
/**
 *
 * @author Hamza
 */
public class EmployeTest {
    
    /**
     * A constructor should be always present
     */
    public EmployeTest(){
        
    }
    
    public void runTests()
    {
        System.out.println("Hey i'm a test for Tests.Employe");
        /*on va ajouter la new connection
        
        EmployeORM dl=new EmployeORM();
        Employe em=new Employe();
        System.out.println("\n");
        System.out.println("AFFICHAGE LISTE NORMALE\n");
        Vector<Employe> vc= new Vector<Employe>();
        vc=dl.list();
        for(int i=0;i<vc.size();i++){
            System.out.println(vc.get(i));
        }
        
        /*
       //AJOUT (enlever les commentaires pour tester)
        
        System.out.println("\n");
        System.out.println("AFFICHAGE AVEC L'AJOUT\n");
        // TEST D'AJOUT
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	String dateNaiss = "1990-11-04";
        String dateEntre = "2014-10-14";
        String dateSorte = "2017-09-16";
	try {

            Date dat1 = formatter.parse(dateNaiss);
            Date dat2 = formatter.parse(dateEntre);
            Date dat3 = formatter.parse(dateSorte);
            em.setNom("Moncif");
            em.setPrenom("Masta");
            em.setDateNaissance(dat1);
            em.setDateEntree(dat2);
            em.setDateSortie(dat3);
            em.setCategorie(1);
            dl.AjouterEmploye(em);//FOnction D'ajout d'un Employe
            dl.list();//AFFICHAGE DE LA LISTE DES EMPLOYES
            System.out.println("DONNEES AJOUTEES AVEC SUCCES");
	} catch (ParseException e) {
		e.printStackTrace();
	}
        
        */
        
        
        
        /*MODIFICATION (enlever les commentaires pour tester)
        
        System.out.println("\n");
        System.out.println("AFFICHAGE DE LA MODIFICATION\n");
        // TEST DE MODIFICATION
	String dateNaiss2 = "1991-11-04";
        String dateEntre2 = "2013-10-14";
        String dateSorte2 = "2019-09-16";
	try {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

            Date dat1 = formatter.parse(dateNaiss2);
            Date dat2 = formatter.parse(dateEntre2);
            Date dat3 = formatter.parse(dateSorte2);
            em.setIdEmploye(3);//on modifie la ligne où l'id=3
            em.setNom("Issam");
            em.setPrenom("Omri");
            em.setDateNaissance(dat1);
            em.setDateEntree(dat2);
            em.setDateSortie(dat3);
            em.setCategorie(1);
            dl.ModifierEmploye(em);//FOnction de modification d'un Employe
            dl.list();//AFFICHAGE DE LA LISTE DES EMPLOYES
            System.out.println("DONNEES MODIFIES AVEC SUCCES");
	} catch (ParseException e) {
		e.printStackTrace();
	}
        
        */
        
        
        /*SUPPRESSION (enlever les commentaires pour tester)
        
        System.out.println("\n");
        System.out.println("AFFICHAGE DE LA SUPPRESSION\n");
        // TEST DE MODIFICATION

            dl.SupprimerEmploye(3);//FOnction De suppression d'un Employe
            dl.list();//AFFICHAGE DE LA LISTE DES EMPLOYES
            System.out.println("DONNEES MODIFIES AVEC SUCCES");
	
        */
        
        
        
        
        CongeORM dl=new CongeORM();
        Conge co=new Conge();
        System.out.println("AFFICHAGE LISTE NORMALE DES CONGES \n");
        Vector<Conge> vc= new Vector<Conge>();
        vc=dl.listByEmploye(1); 
        for(int i=0;i<vc.size();i++){
            System.out.println(vc.get(i));
        }
        System.out.println("test d'uns eul congé");
        System.out.println(dl.find(3));
        
        EmployeORM el= new EmployeORM();
        System.out.println("test d'uns eul employé");
        System.out.println(el.find(2));
    }
    
    
}
