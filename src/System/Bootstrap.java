/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package System;

import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hamza Singleton Design Pattern
 */
public class Bootstrap {

    /**
     * By default, attribute are set to null for this Singleton Class
     */
    private static Bootstrap Instance = null;
    private Database db = null;

    /**
     * Singleton: Force to not instantiate this class
     */
    protected Bootstrap() {}

    /* Static 'instance' method */
    public static Bootstrap getInstance() {
        if (Instance == null) {
            Instance = new Bootstrap();
        }
        return Instance;
    }

    /**
     * Database Initialization
     */
    public void initDatabase() {
        if(db == null)
            db = new Database();
    }


    /**
     * Run class by calling a Name Space
     *
     * @param classNamespace
     * @return Object
     */
    public Object run(String classNamespace) {
        Object o = null;
        try {
            Class c = this.getClass().getClassLoader().loadClass(classNamespace);
            o = c.newInstance();
           
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(Bootstrap.class.getName()).log(Level.SEVERE, null, ex);
        }
        return o;
    }
    
    /**
     * Get Database
     * @return Database
     */
    public Database getDatabase() {
        return db;
    }
    
    /**
     * Get Database Connection
     * @return Connection
     */
    public Connection getDBConnection() {
        return getDatabase().getCon();
    }

}
