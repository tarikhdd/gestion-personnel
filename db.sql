-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Lun 12 Janvier 2015 à 18:32
-- Version du serveur :  5.6.16
-- Version de PHP :  5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `projetjava`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE IF NOT EXISTS `categorie` (
  `idCategorie` int(11) NOT NULL AUTO_INCREMENT,
  `Penalite` int(11) NOT NULL,
  `NbJoursFerie` int(11) NOT NULL,
  `Salaire` float NOT NULL,
  `IntituleCategorie` varchar(100) NOT NULL,
  PRIMARY KEY (`idCategorie`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `categorie`
--

INSERT INTO `categorie` (`idCategorie`, `Penalite`, `NbJoursFerie`, `Salaire`, `IntituleCategorie`) VALUES
(1, 100, 15, 2500, 'Technicien'),
(2, 400, 30, 4000, 'Prof Permanent');

-- --------------------------------------------------------

--
-- Structure de la table `conge`
--

CREATE TABLE IF NOT EXISTS `conge` (
  `idConge` int(11) NOT NULL AUTO_INCREMENT,
  `idEmploye` int(11) NOT NULL,
  `DateDebut` date NOT NULL,
  `DateFin` date NOT NULL,
  PRIMARY KEY (`idConge`),
  KEY `idEmploye` (`idEmploye`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `conge`
--

INSERT INTO `conge` (`idConge`, `idEmploye`, `DateDebut`, `DateFin`) VALUES
(3, 1, '2015-01-07', '2015-01-15'),
(4, 2, '2015-01-13', '2015-01-17');

-- --------------------------------------------------------

--
-- Structure de la table `employe`
--

CREATE TABLE IF NOT EXISTS `employe` (
  `idEmploye` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(100) NOT NULL,
  `Prenom` varchar(100) NOT NULL,
  `DateNaissance` date NOT NULL,
  `DateEntree` date NOT NULL,
  `DateSortie` date NOT NULL,
  `idCategorie` int(11) NOT NULL,
  PRIMARY KEY (`idEmploye`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `employe`
--

INSERT INTO `employe` (`idEmploye`, `Nom`, `Prenom`, `DateNaissance`, `DateEntree`, `DateSortie`, `idCategorie`) VALUES
(1, 'tarik', 'haddadi', '2014-12-01', '2014-12-22', '2014-12-06', 1),
(2, 'ayach', 'oussama', '2014-12-02', '2014-12-17', '2014-12-02', 2);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `conge`
--
ALTER TABLE `conge`
  ADD CONSTRAINT `conge_ibfk_1` FOREIGN KEY (`idEmploye`) REFERENCES `employe` (`idEmploye`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
